const qiniu = require('qiniu')
const axios = require('axios')
const fs = require('fs')

class QiniuManager {

  constructor(accessKey, secretKey, bucket) {
    this.mac = new qiniu.auth.digest.Mac(accessKey, secretKey);
    this.bucket = bucket

    // 初始化配置类
    this.config = new qiniu.conf.Config();
    // 空间对应的机房
    this.config.zone = qiniu.zone.Zone_z0;

    // 文件下载类
    this.bucketManager = new qiniu.rs.BucketManager(this.mac, this.config);
  }

  // 常规回调
  _handleCallback(resolve, reject) {
    return (respErr, respBody, respInfo) => {
      if (respErr) {
        throw respErr;
      }
      if (respInfo.statusCode === 200) {
        resolve(respBody)
      } else {
        reject({
          respInfo: respInfo.statusCode,
          respBody,
        })
      }
    }
  }

  // 获取buckerDomain
  getBucketDomain() {
    const reqURL = `http://uc.qbox.me/v2/domains?tbl=${this.bucket}`
    const digest = qiniu.util.generateAccessToken(this.mac, reqURL)

    return new Promise((resolve, reject) => {
      qiniu.rpc.postWithoutForm(reqURL, digest, this._handleCallback(resolve, reject))
    })
  }

  // 生成下载地址
  generateDownloadLink(key) {
    const domainPromise = this.publicBucketDomain ? Promise.resolve([this.publicBucketDomain])
    : this.getBucketDomain()
    return domainPromise.then(res => {
      if (Array.isArray(res) && res.length > 0) {
        const pattern = /^http?/
        this.publicBucketDomain = pattern.test(res[0]) ? res[0] : `http://${res[0]}`
        return this.bucketManager.publicDownloadUrl(this.publicBucketDomain, key)
      }
      else{
        throw Error('域名未找到，请查看存储空间是否过期！')
      }
    })
  }

  // 文件上传
  uploadFile(key, locakFilePath) {
    const options = {
      scope: this.bucket + ":" + key,
    }
    const putPolicy = new qiniu.rs.PutPolicy(options)
    const uploadToken=putPolicy.uploadToken(this.mac)

    const formUploader = new qiniu.form_up.FormUploader(this.config)
    const putExtra = new qiniu.form_up.PutExtra();

    return new Promise((resolve, reject) => {
      formUploader.putFile(uploadToken, key, locakFilePath, putExtra, this._handleCallback(resolve, reject))
    })
  }

  // 文件远程删除
  deleteFile(key) {
    return new Promise((resolve, reject) => {
      this.bucketManager.delete(this.bucket, key, this._handleCallback(resolve, reject))
    })
  }

  // 文件下载
  downloadFile(key, downloadPath) {
    // 获取下载链接
    // 发送请求,希望返回一个可读流
    // 创建一个可写流，将可读流写入
    // 返回一个promise
    return this.generateDownloadLink(key)
    .then(link => {
      const timeStamp = new Date().getTime()
      const url = `${link}?timeStamp=${timeStamp}`
      return axios({
        url,
        methon: 'GET',
        responseType: 'stream',
        headers: {'X-Custom-Header': 'no-cache'}
      })
    })
    .then(response => {
      const writer = fs.createWriteStream(downloadPath)
      console.log(response.data)
      response.data.pipe(writer)
      return new Promise((resolve, reject) => {
        writer.on('finish', resolve)
        writer.on('error', reject)
      })
    })
    .catch(err => {
      return Promise.reject({err: err.response})
    })
  }

  // 获取云端文件
  getStat(key) {
    return new Promise((resolve, reject) => {
      this.bucketManager.stat(this.bucket, key, this._handleCallback(resolve, reject))
    })
  }
}

module.exports = QiniuManager
