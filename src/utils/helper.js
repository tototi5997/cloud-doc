// 数组转对象
export const flattenArr = (arr) => {
  return arr.reduce((map, item) => {
    map[item.id] = item
    return map
  }, {})
}

// 对象转数组
export const objToArr = (obj) => {
  return Object.keys(obj).map(key => obj[key])
}

// 获取父节点
export const getParentNode = (node, parentClassName) => {
  let current = node
  while(current !== null) {
    if (current.classList.contains(parentClassName)) {
      return current
    }
    current = current.parentNode
  }
  return false
}

// 时间戳
export const timeStampToString = (timeStamp) => {
  const date = new Date(timeStamp)
  return date.toLocaleDateString() + ' ' + date.toLocaleTimeString()
}