import { useState } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { faPlus, faFileImport, faSave } from '@fortawesome/free-solid-svg-icons'
// import defaultFiles from './components/file-list/data'
import { flattenArr, objToArr, timeStampToString} from './utils/helper'
import FileSearch from './components/file-search'
import BottomBtn from './components/bottom-btn'
import SimpleMDE from "react-simplemde-editor"
import FileList from './components/file-list'
import Tablist from './components/tablist'
import Loader from './components/loader'
import "easymde/dist/easymde.min.css"
import {v4 as uuid} from 'uuid'
import fileHelper from './utils/fileHelper'
import useIpcRenderer from './hooks/useIpcRenderer'

const { join, basename, extname, dirname } = window.require('path')
const { remote, ipcRenderer } = window.require('electron')
const Store = window.require('electron-store')

const fileStore = new Store({'name': 'Files Data'})
const settingsStore = new Store({ name: 'Settings'})

const saveFilesToStore = (files) => {
  // 不需要把所有的信息都存到store中
  const filesStoreObj = objToArr(files).reduce((result, file) => {
    const { id, path, title, createAt, isSynced, updateAt } = file
    result[id] = {
      id,
      path,
      title,
      createAt,
      isSynced,
      updateAt,
    }
    return result
  }, {})
  fileStore.set('files', filesStoreObj)
}


function App() {
  const [files, setFiles] = useState(fileStore.get('files') || {})
  const [activeFildId, setActiveFileId] = useState('')
  const [openFileIds, setOpenFileIds] = useState([])
  const [unsavedFileIds, setUnsavedFileIds] = useState([])
  const [searchFiles, setSearchFiles] = useState([])
  const [isLoading, setLoading] = useState(false)

  const filesArr = objToArr(files)
  const savedLocation = settingsStore.get('savedFileLocation') || remote.app.getPath('documents')

  // 是否打开并配置了自动同步参数
  const getAutoSync = () => ['accessKey', 'secretKey', 'bucketName', 'enableAutoSync'].every(key => !!settingsStore.get(key))
  const opendFiles = openFileIds.map(openId => {
    return files[openId]
  })
  // 当前激活的file
  const activeFile = files[activeFildId]
  const filesListArr = searchFiles.length > 0 ? searchFiles : filesArr

  // 单击file
  const fileClick = fileId => {
    setActiveFileId(fileId)
    // add new file to openfile
    const currentFile = files[fileId]
    const { id, title, path, isLoaded} = currentFile
    if (!isLoaded) {
      if (getAutoSync()) {
        ipcRenderer.send('download-file', {key: `${title}.md`, path, id})
      } else {
        fileHelper.readFile(path).then(value => {
          const newFile = {...files[fileId], body: value, isLoaded: true}
          setFiles({...files, [fileId]: newFile})
        })
      }
    }
    if (!openFileIds.includes(fileId)) {
      setOpenFileIds([...openFileIds, fileId])
    }
  }

  // 单击tab
  const tabClick = fileId => {
    setActiveFileId(fileId)
  }

  // 关闭tab
  const tabClose = id => {
    const tabsWithout = openFileIds.filter(fileId => fileId !== id)
    setOpenFileIds(tabsWithout)
    if (tabsWithout.length) {
      setActiveFileId(openFileIds[0])
    } else {
      setActiveFileId('')
    }
  }

  // 文件变化
  const fileChange =(id, value) => {
    if (value !== files[id].body) {
      const newFile = { ...files[id], body: value }
      setFiles({ ...files, [id]: newFile })
      // update unsavedIDs
      if (!unsavedFileIds.includes(id)) {
        setUnsavedFileIds([ ...unsavedFileIds, id])
      }
    }
  }

  // 删除 文件
  const deleteFile = id => {
    if (files[id].isNew) {
      const {[id]: value, ...afterDelete} = files
      setFiles(afterDelete)
    } else {
      fileHelper.deleteFile(files[id].path).then(() => {
        const {[id]: value, ...afterDelete} = files
      setFiles(afterDelete)
        saveFilesToStore(afterDelete)
        // close tab if open
        tabClose(id)
      })
    }
  }

  // 更新文件名
  const updateFileName = (id, title, isNew) => {

    const newPath = isNew ? join(savedLocation, `${title}.md`)
    : join(dirname(files[id].path), `${title}.md`)

    const modifiedFile = {...files[id], title, isNew: false, path: newPath}
    const newFile = {...files, [id]: modifiedFile}

    if (isNew) {
      // 新文件，需要新建文件
      fileHelper.writeFile(newPath, files[id].body)
      .then(() => {
        setFiles(newFile)
        saveFilesToStore(newFile)
      })
      .catch(err => {console.log(err)})
    } else {
      // 旧文件，更新文件名即可
      const oldPath = files[id].path
      fileHelper.renameFile(oldPath, newPath).then(() => {
        setFiles(newFile)
        saveFilesToStore(newFile)
      })
    }
  }

  // 搜索
  const fileSearch = keyWord => {
    const newFiles = filesArr.filter(file => file.title.includes(keyWord))
    setSearchFiles(newFiles)
  }

  // 新建文件
  const createNewFile = () => {
    const newId = uuid()
    const newFile = {
      id:newId,
      title: '',
      body: '## 请输入MarkDown',
      createdAt: new Date().getTime(),
      isNew: true,
    }
    setFiles({...files, [newId]: newFile})
  }

  // 保存文件
  const saveCurrentFile = () => {
    const {path ,body, title } = activeFile
    fileHelper.writeFile(path, body).then(() => {
      setUnsavedFileIds(unsavedFileIds.filter(id => id !== activeFile.id))
      if (getAutoSync()) {
        ipcRenderer.send('upload-file', {key: `${title}.md`, path})
      }
    })
  }

  // 导入文件
  const importFiles = () => {
    remote.dialog.showOpenDialog({
      title: '选择导入的MarkDown文件',
      properties: ['openFile', 'multiSelections'],
      filters: [
        {name: 'MarkDown files', extensions: ['md']}
      ]
    }, (paths) => {
      // 取消返回undefined，先判断
      if(Array.isArray(paths)) {
        // 去重复
        const filteredPaths = paths.filter(path => {
          const alreadyAdded = Object.values(files).find(file => {
            return file.path === path
          })
          return !alreadyAdded
        })
        // 构造一个相同的结构的数据来保存在store中 [{id: 1, path: '', title: ''}]
        const importFilesArr = filteredPaths.map(path => {
          return {
            id: uuid(),
            title: basename(path, extname(path)),
            path,
          }
        })
        // 更新一个files来渲染文件列表
        const newFiles = {...files, ...flattenArr(importFilesArr)}
        setFiles(newFiles)
        saveFilesToStore(newFiles)
        // 告诉用户导入了多少个文件，dialog
        if (importFilesArr.length > 0) {
          remote.dialog.showMessageBox({
            type: 'info',
            title: '导入成功',
            message: `成功导入${importFilesArr.length}了个文件！`
          })
        }
      }
    })
  }

  // 更新本地状态
  const activeFileUploaded = () => {
    const {id} = activeFile
    const modifiedFile = {...files[id], isSynced: true, updateAt: new Date().getTime()}
    const newFiles = {...files, [id]: modifiedFile}
    setFiles(newFiles)
    saveFilesToStore(newFiles)
  }

  // 远端最新文件更新
  const activeFileDownloaded = (event, message) => {
    const currentFile = files[message.id]
    const {id, path} = currentFile
    fileHelper.readFile(path).then(value => {
      let newFile
      if (message.status === 'downloaded-success') {
        newFile = { ...files[id], body: value, isLoaded: true, isSynced: true, updateAt: new Date().getTime()}
      } else {
        newFile = { ...files[id], body: value, isLoaded: true}
      }
      const newFiles = { ...files, [id]: newFile}
      setFiles(newFiles)
      saveFilesToStore(newFiles)
    })
  }

  const filesUploaded = () => {
    const newFiles = objToArr(files).reduce((result, file) => {
      const currentTime = new Date().getTime()
      result[file.id] = {
        ...files[file.id],
        isSynced: true,
        updateAt: currentTime
      }
      return result
    }, {})
    setFiles(newFiles)
    saveFilesToStore(newFiles)
  }

  useIpcRenderer({
    'create-new-file': createNewFile,
    'import-file': importFiles,
    'save-edit-file': saveCurrentFile,
    'active-file-uploaded': activeFileUploaded,
    'file-downloaded': activeFileDownloaded,
    'loading-status': (message, status) => { setLoading(status) },
    'files-uploaded': filesUploaded,
  })

  return (
    <div className="App container-fluid px-0">
      { isLoading && <Loader />}
      <div className="row g-0">
        <div className="col-3 bg-light left-panel">
          <FileSearch
            title="我的云文档"
            onFileSearch={fileSearch}
          />
          <FileList
            files={filesListArr}
            onFileClick={fileClick}
            onFileDelete={deleteFile}
            onSaveEdit={(id, newValue, isNew) => { updateFileName(id, newValue, isNew)}}
          />
          <div className="row g-0 button-group">
            <div className="col d-flex justify-content-center align-items-center">
              <BottomBtn
                text="新建"
                colorClass="btn-primary"
                icon={faPlus}
                onBtnClick={createNewFile}
              />
            </div>
            <div className="col d-flex justify-content-center align-items-center">
              <BottomBtn
                text="导入"
                colorClass="btn-success"
                icon={faFileImport}
                onBtnClick={importFiles}
              />
            </div>
          </div>
        </div>
        <div className="col-9 right-panel">
          {
            !activeFildId && (
              <div className="start-page">
                选择或者创建新的MarkDown文档
              </div>
            )
          }
          {
            activeFildId && (
              <>
                <Tablist
                  files={opendFiles}
                  onTabclick={tabClick}
                  activeId={activeFildId}
                  onCloseTab={tabClose}
                  unsaveIds={unsavedFileIds}
                />
                <SimpleMDE
                  key={activeFile && activeFile.id}
                  value={activeFile && activeFile.body}
                  onChange={value => { fileChange(activeFile.id, value)}}
                  options={{
                    minHeight:'510px'
                  }}
                />
                {
                  activeFile.isSynced &&
                  <span className="sync-status">已同步,上次同步{timeStampToString(activeFile.updateAt)}</span>
                }
                {/* <BottomBtn
                  text="保存"
                  colorClass="btn-success"
                  icon={faSave}
                  onBtnClick={saveCurrentFile}
                /> */}
              </>
            )
          }
        </div>
      </div>
    </div>
  );
}

export default App
