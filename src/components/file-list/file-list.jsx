import React, { useRef, useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash, faTimes } from '@fortawesome/free-solid-svg-icons'
import { faMarkdown } from '@fortawesome/free-brands-svg-icons'
import useKeyPress from '../../hooks/useKeyPress'
import useContextMenu from '../../hooks/useContextMenu'
import { getParentNode  } from '../../utils/helper'

const { remote } = window.require('electron')
const { Menu, MenuItem} = remote

const FileList = ({files, onFileClick, onSaveEdit, onFileDelete}) => {

  const [editState, setEditState] = useState(false)
  const [value, setValue] = useState('')
  const inputRef = useRef(null)
  const enterPressed = useKeyPress(13)
  const escPressed = useKeyPress(27)

  // 点击文件
  const clickFile = fileId => {
    onFileClick(fileId)
  }

  // 点击编辑
  const clickEdit = file => {
    setEditState(file.id)
    setValue(file.title)
  }

  // 点击删除
  const clickDelete = fileId => {
    onFileDelete(fileId)
  }

  // 点击item关闭
  const closeSearch = editItem => {
    setEditState(false)
    setValue('')

    // 如果有正在编辑的
    if(editItem.isNew) {
      onFileDelete(editItem.id)
    }
  }

  // useEffect中做input的自动聚焦
  const clickItem = useContextMenu([
    {
      label: '打开',
      click: () => {
        const parentElement = getParentNode(clickItem.current, 'file-item')
        if (parentElement) {
          onFileClick(parentElement.dataset.id)
        }
      }
    },
    {
      label: '重命名',
      click: () => {
        const parentElement = getParentNode(clickItem.current, 'file-item')
        if (parentElement) {
          const currentFile = files.find(file => file.id === parentElement.dataset.id)
          clickEdit(currentFile)
        }
      }
    },
    {
      label: '删除',
      click: () => {
        const parentElement = getParentNode(clickItem.current, 'file-item')
        if (parentElement) {
          clickDelete(parentElement.dataset.id)
        }
      }
    }
  ], '.file-list', [files])

  // 键盘事件
  useEffect(() => {
    const editItem = files.find(file => file.id === editState)
    if (enterPressed && editState && value.trim() !== '') {
      onSaveEdit(editItem.id, value, editItem.isNew)
      setEditState(false)
      setValue('')
    }
    if(escPressed && editState) {
      closeSearch(editItem)
    }
  })

  useEffect(() => {
    const newFile = files.find(file => file.isNew)
    if (newFile) {
      setEditState(newFile.id)
      setValue(newFile.title)
    }
  }, [files])

  return (
    <ul className="list-group list-group-flush file-list">
      {
        files.map(item => (
          <li
            className="list-group-item bg-light d-flex align-items-center file-item row g-0"
            key={item.id}
            data-id={item.id}
            data-title={item.title}
          >
            {
              ((item.id !== editState)&& !item.isNew) && (
                <>
                  <span className="col-2"><FontAwesomeIcon icon={faMarkdown} size="lg"/></span>
                    <span
                      className="col-7 c-link"
                      onClick={() => clickFile(item.id)}
                    >
                      {item.title}
                    </span>
                    <button
                      className="icon-button col-1"
                      onClick={() => clickEdit(item)}
                    >
                      <FontAwesomeIcon icon={faEdit} size="xs"/>
                    </button>
                    <button
                      className="icon-button col-1"
                      onClick={() => clickDelete(item.id)}
                    >
                      <FontAwesomeIcon icon={faTrash} size="xs"/>
                    </button>
                </>
              )
            }
            {
              ((item.id === editState) || item.isNew) && (
                <>
                  <div className="d-flex justify-content-between align-items-center">
                    <input
                      ref={inputRef}
                      className="form-control"
                      value={value}
                      onChange={e => { setValue(e.target.value)}}
                      placeholder="请输入文件名称"
                    />
                    <button
                      type="button"
                      className="icon-button"
                      onClick={() => closeSearch(item)}
                    >
                      <FontAwesomeIcon icon={faTimes} title="关闭" size="xs"/>
                    </button>
                  </div>
                </>
              )
            }
          </li>
        ))
      }
    </ul>
  )
}

export default FileList