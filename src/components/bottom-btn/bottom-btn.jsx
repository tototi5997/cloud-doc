import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const BottomBtn = ({ text, colorClass, icon, onBtnClick}) => {
  return (
    <button
      type="button"
      className={`container-fluid btn btn-block no-border ${colorClass}`}
      onClick={onBtnClick}
    >
      <FontAwesomeIcon
        className="me-2"
        title=""
        size="sm"
        icon={icon}
      />
      {text}
    </button>
  )
}

export default BottomBtn
