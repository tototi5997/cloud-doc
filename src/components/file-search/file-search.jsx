import React, { useEffect, useState, useRef } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faTimes } from '@fortawesome/free-solid-svg-icons'
import useKeyPress from '../../hooks/useKeyPress'
import useIpcRenderer from '../../hooks/useIpcRenderer'

const FileSearch = ({title, onFileSearch }) => {

  const [inputActive, setInputActive] = useState(false)
  const [value, setValue] = useState('')
  const inputRef = useRef(null)
  const enterPressed = useKeyPress(13)
  const escPressed = useKeyPress(27)

  const closeSearch = () => {
    setInputActive(false)
    setValue('')
    onFileSearch('')
  }

  const openSearchInput = () => {
    setInputActive(true)
  }

  useEffect(() => {
    if (enterPressed && inputActive) {
      onFileSearch(value)
    }
    if (escPressed && inputActive) {
      closeSearch()
    }
  })

  useEffect(() => {
    if (inputActive) {
      inputRef.current.focus()
    }
  }, [inputActive])

  const clickSearch = e => {
    setValue(e.target.value)
  }

  useIpcRenderer({
    'search-file': openSearchInput,
  })

  return (
    <div className="alert alert-primary mb-0">
      {
        !inputActive &&
        <div className="d-flex justify-content-between align-items-center">
          <span>{title}</span>
          <button
            type="button"
            className="icon-button"
            onClick={openSearchInput}
          >
            <FontAwesomeIcon icon={faSearch} title="搜索" size="lg"/>
          </button>
        </div>
      }
      {
        inputActive && (
          <div className="d-flex justify-content-between align-items-center">
            <input
              ref={inputRef}
              className="form-control"
              value={value}
              onChange={clickSearch}
            />
            <button
              type="button"
              className="icon-button"
              onClick={closeSearch}
            >
              <FontAwesomeIcon icon={faTimes} title="关闭" size="lg"/>
            </button>
          </div>
        )
      }
    </div>
  )
}

export default FileSearch