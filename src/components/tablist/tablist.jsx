import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import c from 'classnames'
import './tablist.scss'

const Tablist = ({ files, activeId, unsaveIds, onTabclick, onCloseTab}) => {

  return (
    <ul className="nav nav-pills tablist-component">
      {
        files.map(file => {
          const withUnsavedMark = unsaveIds.includes(file.id)
          const fClassname = c('nav-link',{
            'active': file.id === activeId,
            'withUnsaved': withUnsavedMark,
          })

          return (
            <li className="nav-item" key={file.id}>
              <a
                href="##"
                className={fClassname}
                onClick={e => {
                  e.preventDefault()
                  onTabclick(file.id)
                }}
              >
                {file.title}
                <span
                  className="ms-2 close-icon"
                  onClick={e => {
                    e.stopPropagation()
                    onCloseTab(file.id)
                  }}
                >
                  <FontAwesomeIcon
                    icon={faTimes}
                    size="sm"
                  />
                </span>
                {
                  withUnsavedMark && (
                    <span
                      className="rounded-circle unsaved-icon ms-2"
                    />
                  )
                }
              </a>
            </li>
          )
        })
      }
    </ul>
  )
}

export default Tablist
