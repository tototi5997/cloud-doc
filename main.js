const { app, ipcMain, Menu, dialog } = require('electron')
const isDev = require('electron-is-dev')
const menuTemplate = require('./src/menuTemplate')
const path = require('path')
const AppWindow = require('./src/AppWindow')
const Stroe = require('electron-store')
const QiniuManager = require('./src/utils/QiniuManager')

const settingsStore = new Stroe({name: 'Settings'})
const fileStore = new Stroe({name: 'Files Data'})
let mainWindow, settingsWindow

const createManager = () => {
  const accessKey = settingsStore.get('accessKey')
  const secretKey = settingsStore.get('secretKey')
  const bucketName = settingsStore.get('bucketName')
  return new QiniuManager(accessKey, secretKey, bucketName)
}

app.on('ready', () => {
  const mainWindowConfig = {
    width: 1580,
    height: 745,
  }
  const urlLocation = isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, './build/index.html')}`
  mainWindow = new AppWindow(mainWindowConfig, urlLocation)
  mainWindow.on('closed', () => {
    mainWindow = null
  })
  // 设置菜单
  let menu = Menu.buildFromTemplate(menuTemplate)
  Menu.setApplicationMenu(menu)

  // 主进程事件
  ipcMain.on('open-setting-window', () => {
    const settingWindowConfig = {
      width: 500,
      height: 400,
      parent: mainWindow,
    }
    const settingsFileLocation = `file://${path.join(__dirname, './settings/settings.html')}`
    settingsWindow = new AppWindow(settingWindowConfig, settingsFileLocation)
    settingsWindow.removeMenu()
    settingsWindow.on('closed', () => {
      settingsWindow = null
    })
  })

  // 动态渲染菜单事件
  ipcMain.on('config-is-saved', () => {
    // 注意，菜单index在mac和window不一样
    let qiniuMenu = process.platform === 'darwin' ? menu.items[3] : menu.items[2]
    const switchItems = (toggle) => {
      [1, 2, 3].forEach(number => {
        qiniuMenu.submenu.items[number].enabled = toggle
      })
    }
    const qiniuisConfig = ['accessKey', 'secretKey', 'bucketName'].every(key => !!settingsStore.get(key))
    switchItems(qiniuisConfig)
  })

  // 本地保存自动同步云端文件
  ipcMain.on('upload-file', (event, data) => {
    const manager = createManager()
    manager.uploadFile(data.key, data.path)
    .then(data => {
      console.log('上传成功', data)
      mainWindow.webContents.send('active-file-uploaded')
    })
    .catch(err => {
      dialog.showErrorBox('同步失败', '请检查设置参数是否正确')
    })
  })

  // 本地更新云端文件（下载操作）
  ipcMain.on('download-file', (evetn, data) => {
    const manager = createManager()
    const filesObj = fileStore.get('files')
    const { key, path, id } = data
    manager.getStat(data.key)
    .then((resp) => {
      const saverUpdatedTime = Math.round(resp.putTime / 10000)
      const localupdateTime = filesObj[data.id].updateAt
      if (saverUpdatedTime > localupdateTime || !localupdateTime) {
        manager.downloadFile(key ,path).then(() => {
          mainWindow.webContents.send('file-downloaded', {status: 'downloaded-success', id})
        })
      } else {
        mainWindow.webContents.send('file-downloaded', {status: 'no-new-file', id})
      }
    })
    .catch(err => {
      if (err.respInfo === 612) {
      mainWindow.webContents.send('file-downloaded', {status: 'no-file', id})
      }
    })
  })

  // 本地上传所有文件到云端
  ipcMain.on('upload-all-to-qiniu', () => {
    mainWindow.webContents.send('loading-status', true)
    const manager = createManager()
    const filesObj = fileStore.get('files') || {}
    const uploadPrimiseArr = Object.keys(filesObj).map(key => {
      const file = filesObj[key]
      return manager.uploadFile(`${file.title}.md`, file.path)
    })
    Promise.all(uploadPrimiseArr)
    .then(result => {
      console.log(result)
      dialog.showMessageBox({
        type: 'info',
        info: `成功上传了${result.length}个文件`,
        message: `成功上传了${result.length}个文件`,
      })
      mainWindow.webContents.send('files-uploaded')
    })
    .catch(err => {
      dialog.showErrorBox('上传失败', '请检查设置参数是否正确')
    })
    .finally(() => {
      mainWindow.webContents.send('loading-status', false)
    })
  })
})