const { remote, ipcRenderer } = require('electron')
const Store = require('electron-store')
const qiniuConfigArr = ['#savedFileLocation','#accessKey', '#secretKey', '#bucketName']

const $ = (selector) => {
  const result = document.querySelectorAll(selector)
  return result.length > 1 ? result : result[0]
}

const settingsStore = new Store({ name: 'Settings'})

document.addEventListener('DOMContentLoaded', () => {

  let savedLocation = settingsStore.get('savedFileLocation')
  // 将保存的文件存储路径在页面刷新的时候重新赋值给输入框
  if (savedLocation) {
    $('#savedFileLocation').value = savedLocation
  }
  // 将用户曾经设置过得token相关信息重新赋值给输入框
  qiniuConfigArr.forEach(selector => {
    const savedValue = settingsStore.get(selector.substring(1))
    if (savedValue) {
      $(selector).value = savedValue
    }
  })
  // 点击选择文件选择文件存储路径
  $('#select-new-location').addEventListener('click', () => {
    remote.dialog.showOpenDialog({
      properties: ['openDirectory'],
      message: '选择文件的存储路径',
    }, (path) => {
      if(Array.isArray(path)) {
        $('#savedFileLocation').value = path[0]
      }
    })
  })

  $('#settings-form').addEventListener('submit', (e) => {
    e.preventDefault()
    qiniuConfigArr.forEach(selector => {
      if ($(selector)) {
        let { id, value } = $(selector)
        settingsStore.set(id, value ? value : '')
      }
    })
    // 发送消息到主进程动态渲染菜单状态
    ipcRenderer.send('config-is-saved')
    remote.getCurrentWindow().close()
  })

  // tab添加点击切换事件
  $('.nav-tabs').addEventListener('click', e => {
    e.preventDefault()
    $('.nav-link').forEach(element => {
      element.classList.remove('active')
    })
    e.target.classList.add('active')
    $('.config-area').forEach(element => {
      element.style.display = 'none'
    })
    $(e.target.dataset.tab).style.display = 'block'
  })
})